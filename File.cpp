#include "File.h"
#include <QDir>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include<QJsonArray>

File::File(QObject *parent)
{

}

File::File(QString path, QString url, int choose)
{

    if(Save_place_save_file(path)){
        this->Set_path(path);
    }

    this->Set_filename(Format_filename(saveFileName(QUrl(url)), choose));
}

void File::Set_path(QString Path)
{
    this->place_save_file = Path;
}


void File::Set_filename(QString filename)
{
    this->filename = filename;
}

QString File::Take_filename()
{
    return this->filename;
}

QString File::Take_path()
{
    return this->place_save_file;
}

bool File::Save_place_save_file(QString place_save_file)
{
    //tao duong dan de luu file
    QDir dir(place_save_file);

    if(dir.mkpath(place_save_file) == false){
      //xac dinh cho luu file
      qDebug() <<"Cannot create this path, we set default path.";
      return false;
    }else{
        if(!dir.exists())
            dir.mkpath(place_save_file);
    }

    return true;
}

QString File::saveFileName(const QUrl &url)
{
    QString path = url.path();
    QString basename = QFileInfo(path).fileName();

    if(basename.isEmpty()){
        basename = "Dowloadfile";
    }

    if(QFile::exists(basename)){
         int index = 0;
         basename += '.';
         while(QFile::exists(basename + QString::number(index)))
             index++;
         basename += QString::number(index);
    }

    return basename;
}

QString File::Format_filename(QString filename, int choose)
{

    switch(choose)
    {
        case 1:
             filename += ".json";
             break;
        default:
            break;
    }

    return filename;
}

QStringList File::Read_file_json()//chua duong dan va ten file
{

       QFile file;
       QString listfilejson;
       QStringList Listurlimage;//list string img

       file.setFileName(this->place_save_file + "/" + this->filename);

       if(!file.open(QIODevice::ReadOnly | QIODevice::Text)){
           qWarning() << file.errorString();
       }

       listfilejson = file.readAll();
       file.close();
       QJsonDocument document = QJsonDocument::fromJson(listfilejson.toUtf8());

       QJsonArray arrayimg = document.array();

       foreach (const QJsonValue object, arrayimg) {
           QJsonObject obj = object.toObject();
           Listurlimage.append(obj["url"].toString());
       }
       //remove file

       if(!file.remove())
           qWarning() << "Co van de khi xoa file.";

       return Listurlimage;
}

void File::Removefile(QString path, QString filename)
{
     QFile file;
     file.setFileName(path + "/" + filename);
     if(!file.remove())
         qWarning() << "Co van de khi xoa file.";
}

QString File::Check_path(QString path)
{
    int pos = 1;
    QString pathone = path;
    while(true){
        if(!QDir(pathone).exists()){
           QDir().mkdir(pathone);
           break;
        }

        pathone = path + "_" + QString::number(pos);
        pos++;
    }

    return pathone;
}
