import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Download file")

    Connections{
        target: connectqml
        onInfordownload:{
            blabla.visible = true;
            information.text = "Path file: " + namefile + "\n" +
                               "Ten file duoc luu: " + path
        }

        onProgress_download_to_qml:{
            bar.value = bytes*100
            information.text = "  ###########   Dowloading...   ###########\n" +
                                "  ###########   " + bar.value.toString() + "%   ###########\n" +
                                "  ###########   " + speed + "   ###########\n"
        }
    }

    Column{
        x: 150
        y: 40

        Row{

            Rectangle{
                height: 40
                width: 150

                Rectangle{

                    height: 25
                    width: 60
                    color: "#F0F8FF"
                    radius: 4
                    border.width: 0

                    Text {
                        anchors.centerIn: parent
                        text: "Save file"
                        font.italic: true
                        font.pointSize: 10
                    }
                }

                TextField {
                    x: 70
                    id: save
                    text: "E:/Thuc Tap He 2017/Qt basic/data"
                    placeholderText: qsTr("Choose place save file...")
                    style: TextFieldStyle {
                        textColor: "black"
                         background: Rectangle {
                                radius: 2
                                implicitWidth: 220
                                implicitHeight: 25
                                border.color: "#333"
                                border.width: 1
                        }

                        placeholderTextColor: "red"
                    }

                    onTextChanged: {
                      if(save.text == ""){
                          notify.text = "<b>**</b>Not allow empty save file field."
                      }else{
                           notify.text = "<b>**</b>"
                      }
                    }
                }
            }
       }

       Row{

            y:10

            Rectangle{
                height: 40
                width: 150

                Rectangle{
                    height: 30
                    width: 60
                    color: "orange"
                    radius: 4

                    Text {
                            anchors.centerIn: parent
                            text: "Enter url"
                            font.pointSize: 10
                    }
                }

                TextField {

                    x: 70
                    id: url
                    placeholderText: qsTr("Enter url here...")
                   // text: "http://dep.anh9.com/imgs/14111hinh-nen-Google-danh-cho-trang-web.jpg"
                   // text:"http://effigis.com/wp-content/uploads/2015/02/Airbus_Pleiades_50cm_8bit_RGB_Yogyakarta.jpg"
                      text:"http://localhost:5050/api/data/all"
                    style: TextFieldStyle {
                        textColor: "black"
                        background: Rectangle {
                            radius: 2
                            implicitWidth: 220
                            implicitHeight: 30
                            border.color: "#333"
                            border.width: 1
                        }

                        placeholderTextColor: "red"
                    }

                    onTextChanged: {
                        if(url.text == ""){
                            notify.text = "<b>**</b>Not allow empty url field."
                        }else{
                             notify.text = "<b>**</b>"
                        }
                    }
                }
            }
       }

       Row{
           ProgressBar{
              value: 0
              id: bar
              maximumValue: 100
              minimumValue: 0
              y:20
              x:100
            }
       }

       Row{
           Text{
               y:30
               x:100
               id: notify
               wrapMode: Text.WordWrap
               text: "<b>**</b>"

           }
       }

       Row{
           x:100
           Button{
               id: blabla
               property string track_url: ""
               y:40
               height: 35
               width: 120
               text: "Download"
               onClicked: {
                   if(save.text != "" && url.text != ""){
                       if(blabla.track_url==""){
                            blabla.track_url = url.text
                            connectqml.Set_path(save.text)
                            connectqml.Set_link_download(url.text)
                            connectqml.Set_choice(1)//--download file ket noi server
                          //  connectqml.Set_choice(0)//Set_choice(0)--download file don thuan

                            connectqml.Signal_download()
                            blabla.visible = false;
                       }else{
                           if(blabla.track_url != url.text){
                               blabla.track_url = url.text
                               connectqml.Set_path(save.text)
                               connectqml.Set_link_download(url.text)
                               connectqml.Set_choice(0)
                               connectqml.Signal_download()
                               blabla.visible = false;
                           }else
                              notify.text = "<b>**</b>We just downloaded this url."

                       }
                       information.text = "Information..."

                   }else{
                       if(save.text == ""){
                           notify.text = "<b>**</b>Not allow empty place save file field."
                           save.focus = true
                       }

                       if(url.text == ""){
                           notify.text = "<b>**</b>Not allow empty url field."
                           url.focus = true
                       }
                   }
               }
           }
       }

    }

    Row{
        y:140
        x: 100

        TextArea {
            x: 60
            y: 100
            id: information
            text: "Information.... \n"
            focus: false
            readOnly: true

            z: 2808
         //   opacity: 0.7
            width: 400
            height: 160
            smooth: true
            wrapMode: TextEdit.Wrap
            textColor: "blue"
        }
    }

}
