#ifndef DOWNLOAD_FILE_H
#define DOWNLOAD_FILE_H

#include <QObject>
#include <QFile>
#include <QTime>
#include <QUrl>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QString>
#include <QNetworkDiskCache>

class Download_file : public QObject
{
    Q_OBJECT

public:
    Download_file(QObject * parent = 0);
    Download_file(QString, QString, QString, int);

    QUrl Get_url();
    void Set_url(QString url);
    QString Get_place_save_file();
    void Set_place_save_file(QString place_save_file);
    QString Get_filename();
    void Set_filename(QString filename);
    void Stop_download();
    int process;
    const QString cachename = "cacheDir";
    int cachesize;
    int Cachesize();

signals:
    void finished();
    void finished(int);
    void finished(QNetworkReply *reply);
    void progress_download(qint64, qint64, int, double);
    void Error_download_failed(QString, QString, QString);

public slots:
   void downloadProgress(qint64, qint64);
   void startNextdownload();
   void downloadFinished();
   void downloadReadyRead();
   void replyFinished(QNetworkReply *reply);

private:
    QNetworkAccessManager *manager;

    QNetworkReply *currentDownload;
    QFile output;
    QTime *downloadTime;//tien trinh-thoi gian download file
    QUrl url;
    QString place_save_file;
    QString filename;
   QNetworkDiskCache *diskCache;
};

#endif // DOWNLOAD_FILE_H
