TEMPLATE = app

QT += qml quick
CONFIG += c++11

SOURCES += main.cpp \
    connect_qml.cpp \
    download_file.cpp \
    File.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    connect_qml.h \
    download_file.h \
    File.h
