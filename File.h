#ifndef FILE_H
#define FILE_H

#include <QObject>
#include <QFile>
#include <QString>
#include <QUrl>

class File : public QObject
{
    Q_OBJECT

public:
    File(QObject * parent = 0);
    File(QString, QString, int choose);

    QString Take_filename();
    void Set_filename(QString filename);
    QString Take_path();
    void Set_path(QString Path);

    QString saveFileName(const QUrl &url);
    bool Save_place_save_file(QString place_save_file);
    QString Format_filename(QString filename, int choose);
    QStringList Read_file_json();//doc file json
    void Removefile(QString, QString);
    QString Check_path(QString path);

signals:

public slots:

private:
    QFile read_file;
    QString place_save_file;
    QString filename;

};

#endif // FILE_H
