#include "download_file.h"
#include <QFileInfo>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QString>
#include <QTimer>
#include <QDir>
#include <QFileInfoList>
#include <QStandardPaths>
#include <QAbstractNetworkCache>
#include <QStorageInfo>
#include <stdio.h>

Download_file::Download_file(QObject *parent)
    :QObject(parent), place_save_file(QString::null)
{

}

int Download_file::Cachesize()
{
    int Size = 0;
    QFileInfoList listpath = QDir::drives();
    for(int index = 0; index < listpath.length(); index++)
    {
        QStorageInfo storage = QStorageInfo(listpath[index].path());
        if (!storage.isReadOnly())
            Size += storage.bytesAvailable()/1000/1000;
    }

    return Size;
}

Download_file::Download_file(QString path, QString url, QString filename, int process)
{
    this->cachesize = Cachesize()*1040;//tinh kich thuoc bo nho cache
    this->Set_place_save_file(path);
    this->Set_filename(filename);
    this->Set_url(url);
    this->process = process;
    this->downloadTime = new QTime();
}

QUrl Download_file::Get_url()
{
    if(!this->url.isEmpty())
        return this->url;

    return QUrl("");
}

void Download_file::Set_url(QString url)//link luu file
{
    this->url = QUrl(url);
}

void Download_file::Set_place_save_file(QString place_save_file)
{
    this->place_save_file = place_save_file;
}

QString Download_file::Get_place_save_file()
{
    return this->place_save_file;
}

QString Download_file::Get_filename()
{
    return this->filename;
}

void Download_file::Set_filename(QString filename)
{
    this->filename = filename;
}


void Download_file::startNextdownload()
{
     QUrl url = Get_url();

    output.setFileName(Get_place_save_file()+"/" + Get_filename());

    if(!output.open(QIODevice::WriteOnly)){//doc cho luu file--loi he thong
        fprintf(stderr, "Problem opening save file '%s' for download '%s': %s\n",
                qPrintable(filename), url.toEncoded().constData(),
                qPrintable(output.errorString()));

        startNextdownload();
        return;
    }

    //tien hanh dowload file tu tren mang
   //2 câu lệnh ở dưới tương ứng với  manager->get(QNetworkRequest(QUrl(url)));
    QNetworkRequest request(url);
    manager = new QNetworkAccessManager(this);
    diskCache = new QNetworkDiskCache(this);//download về cache
    diskCache->setCacheDirectory(this->cachename);//tao cache neu khong ton tai
    //If the new size is smaller then the current cache size then the cache will call expire().
    diskCache->setMaximumCacheSize(this->cachesize);

    manager->setCache(diskCache);
    currentDownload = manager->get(request);
   // qDebug() <<"toi da: " <<request.KnownHeaders;

     QNetworkRequest request1(url);
     //lay dư lieu tu cache ra truoc khi download tu tren mang
     request1.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QNetworkRequest::PreferCache);
     manager->get(request1);

    connect(currentDownload, SIGNAL(downloadProgress(qint64, qint64)),
            SLOT(downloadProgress(qint64, qint64)));

    connect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(replyFinished(QNetworkReply*)));

    connect(currentDownload, SIGNAL(finished()),
            SLOT(downloadFinished()));

    //void QIODevice::readyRead() là  phát ra mỗi lần dữ liệu
   //mới có sẵn để đọc từ kênh đọc hiện tại của thiết bị
    connect(currentDownload, SIGNAL(readyRead()),
            SLOT(downloadReadyRead()));

    printf("Downloading %s...\n", url.toEncoded().constData());
            // prepare the output
    downloadTime->start();//tính thời gian dowload file

}

void Download_file::downloadReadyRead()
{
     output.write(currentDownload->readAll());//ghi vao file
}

void Download_file::downloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
    emit progress_download(bytesReceived, bytesTotal, this->process, downloadTime->elapsed());//bắn tín hiệu ra qml để bắt sự kiện
}

void Download_file::downloadFinished()
{
    output.close();

    if (currentDownload->error()) {
        // download failed
        fprintf(stderr, "Failed: %s\n", qPrintable(currentDownload->errorString()));
        emit Error_download_failed(currentDownload->errorString(), this->place_save_file, this->filename);
    } else {
        printf("Succeeded.\n");
    }

    currentDownload->deleteLater();

    emit finished();//ban tin hieu ket thuc xong qua trinh download file
    emit finished(this->process);//ban tin hieu ket thuc xong qua trinh download file

    return;
}

void Download_file::Stop_download()
{
    currentDownload->abort();
    qDebug()<<"Stop_download() >>> Stop download file";
    emit finished();
}

void Download_file::replyFinished(QNetworkReply *reply)
{
    QVariant fromCache = reply->attribute(QNetworkRequest::SourceIsFromCacheAttribute);
    qDebug() << "page from cache?" << fromCache.toBool();
    emit progress_download(reply->header(QNetworkRequest::ContentLengthHeader).toUInt(), reply->header(QNetworkRequest::ContentLengthHeader).toUInt(), this->process, downloadTime->elapsed());
}
